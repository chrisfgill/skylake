use std::collections::HashMap;
use ::kinds::Kinds;

#[derive(Debug)]
pub struct Env {
    pub vars: HashMap<String,Vars>, // NOTE: consider using vec<u8> for sym name
    pub ns: String, //NOTE: store as bytes?
}

impl Env {
    pub fn new () -> Env {
        Env { vars: HashMap::new(),
              ns: String::new() }
    }
}

/// byte encoding table for vars
// 0 reserved
pub const BOOL: u8 = 1u8;
pub const INUM: u8 = 2u8;
pub const FNUM: u8 = 3u8;
pub const REF: u8 = 4u8;
pub const STRING: u8 = 5u8;
pub const BYTE: u8 = 6u8;
pub const VEC: u8 = 7u8; // special case!

// wrapped types
#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq,Clone)]
pub enum Vars {
    Bool(bool),
    INum(i32),
    FNum(f64),
    Ref(Ref), // variable reference that exists in env (as string for now)
    String(String),
    Byte(u8),
    Vec(Vec<Vars>),
    Proc(Vec<Ref>,Vec<Kinds>),
    Native(Natives),

    Nil,
}

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq,Clone)]
pub enum Natives {
    Let,
    If,
    While,
    Prn,
    
    Def,
    Defproc,
    Proc,

    Mul,
    Add,
    Div,
    Sub,

    GT,
    LT,
    Eq,

    Vec, //builds a vec of vars
}

impl Natives {
    pub fn to_string (&self) -> String {
        match self {
            &Natives::Let => "let".to_string(),
            &Natives::If => "if".to_string(),
            &Natives::While => "while".to_string(),
            &Natives::Proc => "proc".to_string(),
            &Natives::GT => ">".to_string(),
            &Natives::LT => "<".to_string(),
            &Natives::Def => "def".to_string(),
            &Natives::Prn => "prn".to_string(),
            &Natives::Mul => "*".to_string(),
            &Natives::Add => "+".to_string(),
            &Natives::Sub => "-".to_string(),
            &Natives::Div => "/".to_string(),
            &Natives::Vec => "vec".to_string(),
            _ => panic!("ni natives: {:?}",self)
        }
    }
}

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq,Clone)]
pub enum RefKinds {
    Move,
    Copy,
    Refer,
    Auto,
}

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq,Clone)]
pub struct Ref {
    pub kind: RefKinds,
    pub value: String,
}

impl Ref {
    pub fn default (s: String) -> Ref {
        Ref { kind: RefKinds::Auto,
              value: s }
    }
    pub fn name (&self) -> &str {
        &self.value
    }
}

impl Vars {
    // TODO: add proc parse
    pub fn parse (s: &str) -> Vars {
        let mut is_num = true;
        let mut is_fnum = false;

        if s.chars().count() > 0 { 
            for (i,c) in s.chars().enumerate() {
                if !is_num { break }
                match c { 
                    '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9' => (),
                    '.' => {
                        if !is_fnum { is_fnum = true }
                        else { is_fnum = false } //no 1.345.23 numbers!
                        // NOTE: consider commas as well as decimals
                    },
                    '-' => {
                        if i > 0 {
                            is_num = false
                        }
                        else if s.chars().count() < 2 { is_num = false }
                    },
                    _ => is_num = false,
                }
            }
        }
        else { is_num = false; }
        
        if is_num {
            if is_fnum { Vars::FNum(s.parse().unwrap()) }
            else { Vars::INum(s.parse().unwrap()) }
        }
        else {
            if s.chars().nth(0).unwrap() == '#' {
                if s.chars().count() == 2 {
                    if s.chars().nth(1).unwrap() == 'f' { Vars::Bool(false) }
                    else if s.chars().nth(1).unwrap()  == 't' { Vars::Bool(true) }
                    else { panic!("warn: invalid var type {:?}",s); }
                }
                else if s.chars().count() > 2 {
                    if s.chars().nth(1).unwrap() == 'b' { Vars::Byte(s[2..].parse().unwrap()) }
                    else { panic!("warn ni: var type {:?}",s); } // TODO: impl vec
                }
                else { Vars::Native(Natives::Proc) } //empty
            }
            else if s.chars().nth(0).unwrap() == '@' {
                Vars::Ref(Ref { kind: RefKinds::Copy, value: (s[1..].to_string()) } )
            }
            else if s.chars().nth(0).unwrap() == '~' {
                Vars::Ref(Ref { kind: RefKinds::Move, value: (s[1..].to_string()) } )
            }
            else if s.chars().nth(0).unwrap() == '&' {
                Vars::Ref(Ref { kind: RefKinds::Refer, value: (s[1..].to_string()) } )
            }
            else { // build natives if necessary
                match s {
                    "let" => Vars::Native(Natives::Let),
                    "if" => Vars::Native(Natives::If),
                    "while" => Vars::Native(Natives::While),
                    "proc" => Vars::Native(Natives::Proc), // this is reparsed in parser
                    "vec" => Vars::Native(Natives::Vec),
                    ">" => Vars::Native(Natives::GT),
                    "<" => Vars::Native(Natives::LT),
                    "prn" => Vars::Native(Natives::Prn),
                    "def" => Vars::Native(Natives::Def),
                    "+" => Vars::Native(Natives::Add),
                    "*" => Vars::Native(Natives::Mul),
                    "-" => Vars::Native(Natives::Sub),
                    "/" => Vars::Native(Natives::Div),
                    "true" => Vars::Bool(true),
                    "false" => Vars::Bool(false),
                    _ => Vars::Ref(Ref::default(s.to_string())), //exclude here?
                }
            }
        }
    }
    
    pub fn to_string(&self) -> String {
        match self {
            &Vars::Nil => { "".to_string() }
            &Vars::Bool(ref b) => {
                if *b { "true".to_string() }
                else { "false".to_string() }
            },
            &Vars::INum(ref n) => {
                n.to_string()
            },
            &Vars::FNum(ref n) => {
                n.to_string()
            },
            &Vars::Ref(ref r) => {
                r.value.clone()
            },
            &Vars::String(ref s) => {
                s.clone()
            },
            &Vars::Byte(ref b) => {
                format!("#b{:?}",b).to_string()
            },
            &Vars::Vec(ref v) => {
                let mut s = String::new();
                s.push_str("(vec ");
                for n in v.iter() {
                    s.push_str(&n.to_string());
                    s.push(' ');
                }
                s.pop();
                s.push(')');
                s
            },
            &Vars::Proc(ref args, ref work) => {
                let mut s = String::new();
                s.push_str("(proc ");
                s.push('(');
                for n in args.iter() {
                    s.push_str(&n.value.to_string());
                    s.push(' ');
                }
                s.pop();
                s.push_str(") ");

                s.push('(');
                for n in work.iter() {
                    s.push_str(&n.to_string());
                    s.push(' ');
                }
                s.pop();
                s.push(')');
                
                s
            },
            &Vars::Native(ref nat) => {
                nat.to_string()
            },
            //_ => { panic!("ni! {:?}", self); }
        }
    }
}

use std::fmt::{self, Formatter, Display};
impl Display for Vars {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}",self.to_string())
    }
}
