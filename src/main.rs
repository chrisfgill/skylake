extern crate skylake;
use skylake::{parse,env};
use skylake::eval::Eval;

use std::path::Path;
use std::fs::File;
use std::io::{Read};

fn main () {
    let args: Vec<String> = std::env::args().collect();
    if args.len() > 1 {
        println!("compiling: {}",args[1]);
        if let Some(mut f) = File::open(&Path::new(&args[1])).ok() {
            
            let mut b = vec!();
            let _ = f.read_to_end(&mut b);
            let exp = String::from_utf8(b).unwrap();
            let block = parse::parse(&exp);
            //println!("{:?}",mem);
            
            let env = vec!(env::Env::new());
            let mut result = env::Vars::Nil;
            let scope = Eval::depth(&block,env,&mut result);
            println!("result:{:?}\nscope:{:?}",result,scope);
        }
    }
    else { println!("ni: interactive repl"); return }
    
    

        
        /*
        if let Some(mut f) = File::create(&Path::new("test.skyb")).ok() {
            let b = parse::to_bytes(&r);
            let _ = f.write(&b);
        }

        if let Some(mut f) = File::open(&Path::new("test.skyb")).ok() {
            
            let mut b = vec!();
            let _ = f.read_to_end(&mut b);

            let r2 = parse::from_bytes(&b);
           // println!("{}\n\n{}\n\n",parse::to_string(&r),parse::to_string(&r2));
           // println!("{:?}",r2);
        }*/
}
