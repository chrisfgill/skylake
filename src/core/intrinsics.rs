use ::eval::{Eval,EvalVars};
use ::env::{Env,Vars,Ref,RefKinds};
use ::kinds::Kinds;

use std::mem;


// NOTE: this should move to an io module
pub fn prn<'a> (scope: Vec<Env>, sexp: &Vec<&'a Vars>) -> Vec<Env> { //ResultEnv {
    let mut output = String::new();
    for s in &sexp[1..] {
        match *s {
            &Vars::Ref(ref s) => {
                if let Some(r) = Eval::find_var(&scope,&s.value) {
                    output.push(' ');
                    output.push_str(&r.to_string());
                }
            },
            &Vars::Native(_) => continue,
            _ => { output.push(' ');
                   output.push_str(&s.to_string()); }
        }
    }

    println!("{}",output);

    scope
}

/// this is the function that lets the user define environment variables
pub fn def<'a> (mut scope: Vec<Env>, sexp: &Vec<&'a Vars>, res: &mut Vars) ->  Vec<Env> {
    let val;
    if sexp.len() < 2 {
        println!("err: def bind must be in pair format\n{:?}",sexp);
        return scope
    }
    else if sexp.len() < 3 {
        val = mem::replace(res,Vars::Nil); //move var out of result
    }
    else { val = sexp[2].clone(); }
    
    let var = &sexp[1];
    
    match *var {
        &Vars::Ref(ref v) => {
            let r = scope[0].vars.insert(v.value.clone(),val);
            if let Some(r) = r {
                *res = r;
            }
        },
        _ => {
            println!("err: non-variable binding in def\n{:?}",sexp);
            return scope
        }
    }

    scope
}

pub fn let_bind<'a> (mut scope: Env,
                     sexp: &mut Vec<EvalVars>,) -> Env {
    match sexp.remove(1) {
        EvalVars::Own(Vars::Vec(mut v)) => {
            if v.len() & 1 != 0 {
                println!("err: let bindings must be in pairs:{:?}",v.len());
                return scope
            }

            let mut var = None;
            for (k,n) in v.drain(..).enumerate() {
                match n {
                    Vars::Ref(r) => {
                        var = Some(r);
                    },
                    _ => {
                        if k & 1 == 0 {
                            println!("err: non-variable binding in let\n{:?}",sexp);
                            return scope
                        }
                        else {
                            if let Some(ref var) = var {
                                scope.vars.insert(var.value.clone(),n);
                            }
                        }
                    }
                }
            }
        },
        _ => {}
    }

    //scope = Eval::depth(sexp[2],scope,res);

    scope
}

pub fn proc_bind<'a> (mut scope: Vec<Env>,
                      sexp: &Vec<&'a Vars>,
                      res: &mut Vars) -> Vec<Env> {

    let mut binds_own = vec!();
    for b in &sexp[1..] {
        match *b {
            &Vars::Ref(ref r) => {
                if r.kind == RefKinds::Move {
                    if let Some(val) = Eval::take_var_base(&mut scope,r.name()) {
                        binds_own.push(val);
                        continue
                    }
                }
                else if let Some(val) = Eval::find_var_base(&mut scope,r.name()) {
                    binds_own.push(val.clone());
                    continue
                }

                println!("err: no value found in env\n{:?}",scope);
                binds_own.push(Vars::Nil);
            }
            _ => {}
        }
    }

    

    {
        let mut binds: Option<&Vec<Ref>> = None;
        let mut fun: Option<&Vec<Kinds>> = None;
        match sexp[0] {
            &Vars::Ref(ref r) => {
                if let Some(var) = Eval::find_var_base(&scope,r.name()) {
                    match var {
                        &Vars::Proc(ref b,ref f) => {
                            binds = Some(b);
                            fun = Some(f);
                        }
                        _=> {
                            println!("err: non procedure ref evaluated");
                        }
                    }
                }
            }
            &Vars::Proc(ref b,ref f) => {
                binds = Some(b);
                fun = Some(f);
            }
            _=> {
                println!("err: non procedure ref evaluated");
                return scope
            }
        }

        if binds.is_some() && fun.is_some() {
            let mut env = vec!(Env::new());
            if binds.unwrap().len() != sexp.len() - 1 {
                println!("err: proc bindings must be in pairs\n{:?}\n->\n{:?}",binds,sexp);
            }
            else {
                for (i,s) in sexp[1..].iter().enumerate() {
                    match s {
                        &&Vars::Ref(_) => {
                            let val = binds_own.remove(0);
                            env[0].vars.insert(binds.unwrap()[i].value.clone(),val);
                        },
                        _ => {
                            let val = (*s).clone(); //NOTE: it would be preferable not to clone this
                            env[0].vars.insert(binds.unwrap()[i].value.clone(),val);
                        }
                    }
                }

                Eval::depth(fun.unwrap(),env,res);
            }
        }
    }
    
    scope
}
