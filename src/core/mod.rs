pub mod math;
pub mod intrinsics;

//use ::eval::Eval;
use ::env::{Env,Vars};
pub type ResultEnv = (Vec<Env>,Option<Vars>);
