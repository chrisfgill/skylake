use ::eval::Eval;
use ::env::{Env,Vars};

//use ::core::ResultEnv;

// TODO: create a trait that operates on FNum and INum conditionally


pub fn add (scope: Vec<Env>, sexp: &Vec<&Vars>, res: &mut Vars) -> Vec<Env> {
    let mut total: f64 = 0.;

    let mut is_fnum = false;

    for s in &sexp[1..] {
        if let Some((num,was_fnum)) = get_fnum(&scope,&s) {
            if was_fnum { is_fnum = was_fnum; }
            total += num;
        }
    }

    *res = {
        if is_fnum { Vars::FNum(total) }
        else { Vars::INum(total as i32) }
    };
    
    scope
}

pub fn mul (scope: Vec<Env>, sexp: &Vec<&Vars>, res: &mut Vars) -> Vec<Env> {
    let mut total: f64 = 1.;

    let mut is_fnum = false;
    
    for s in &sexp[1..] {
        if let Some((num,was_fnum)) = get_fnum(&scope,&s) {
            if was_fnum { is_fnum = was_fnum; }
            total *= num;
        }
    }

    *res = {
        if is_fnum { Vars::FNum(total) }
        else { Vars::INum(total as i32) }
    };
    
    scope
}

pub fn sub (scope: Vec<Env>, sexp: &Vec<&Vars>, res: &mut Vars) -> Vec<Env> {
    let mut total: f64 = 0.;

    let mut is_fnum = false;

    let s = &sexp[1];
    if let Some((num,was_fnum)) = get_fnum(&scope,&s) {
        if was_fnum { is_fnum = was_fnum; }
        total = num;
    }
    
    for s in &sexp[2..] {
        if let Some((num,was_fnum)) = get_fnum(&scope,&s) {
            if was_fnum { is_fnum = was_fnum; }
            total -= num;
        }
    }

    *res = {
        if is_fnum { Vars::FNum(total) }
        else { Vars::INum(total as i32) }
    };
    scope
}

pub fn div (scope: Vec<Env>, sexp: &Vec<&Vars>, res: &mut Vars) -> Vec<Env> {
    let mut total: f64 = 1.;

    let mut is_fnum = false;

    let s = &sexp[1];
    if let Some((num,was_fnum)) = get_fnum(&scope,&s) {
        if was_fnum { is_fnum = was_fnum; }
        total = num;
    }
    
    for s in &sexp[2..] {
        if let Some((num,was_fnum)) = get_fnum(&scope,&s) {
            if was_fnum { is_fnum = was_fnum; }
            total /= num;
        }
    }
    
    *res = {
        if is_fnum { Vars::FNum(total) }
        else { Vars::INum(total as i32) }
    };
    scope
}


pub fn less_great_than (scope: Vec<Env>, sexp: &Vec<&Vars>, greater: bool, res: &mut Vars) -> Vec<Env> {
    let mut last_num = None;
    let mut result = false;
    
    for s in &sexp[1..] {
        if let Some((num,_)) = get_fnum(&scope,&s) {
            if let Some(last) = last_num {
                if greater {
                    result = last > num;
                }
                else {
                    result = last < num;
                }

                if !result { break }
            }

            last_num = Some(num);
        }
        else { panic!("err: not a number!"); }
    }
    
    *res = Vars::Bool(result);
    scope
}

// returns number if it is a number, and true if it was originally a float
pub fn get_fnum (scope: &Vec<Env>, s: &Vars) -> Option<(f64,bool)> {
    match s {
        &Vars::FNum(n) => {
            return Some((n,true))
        },
        &Vars::INum(n) => {
            return Some((n as f64,false));
        },
        &Vars::Ref(ref s) => {
            if let Some(r) = Eval::find_var_base(&scope,&s.value) {
                match r {
                    &Vars::FNum(n) => {
                        return Some((n,true))
                    },
                    &Vars::INum(n) => {
                        return Some((n as f64,false));
                    },
                    _ => return None,
                }
            }
            else { println!("not found in env scope! {:?}\n{:?}",s,scope); }
            
        },
        _ => {}
    }

    None
}
