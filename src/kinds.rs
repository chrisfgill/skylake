// NOTE: this file is currently unused and may not be of use soon

use byteorder::{ByteOrder, BigEndian};

use ::env::{self,Vars,Ref};

/// byte encoding header-byte
// 0 reserved
pub const SEXP_B: u8 = 1u8; //begin sexp
pub const SEXP_E: u8 = 2u8; //end sexp
pub const SYM: u8 = 3u8;
pub const QSYM: u8 = 4u8;


#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq,Clone)]
pub enum Kinds {
    Sym(Vars),
    //QSym(String), //TODO: consider impl for delayed evaluation of quoted symbols
    Sexp(Vec<Kinds>),
}

impl Kinds {
    /// parses non-quoted strings as symbols with Var kinds
    pub fn parse (s: &str) -> Kinds {
        let var = Vars::parse(s);
        Kinds::Sym(var)
    }
    
    pub fn to_bytes (&self) -> Vec<u8> {
        let mut v: Vec<u8> = vec!();
        match self {
            &Kinds::Sym(ref sym) => {
                v.push(SYM);
                match sym {
                    &Vars::INum(n) => {
                        v.push(env::INUM);
                        
                        let b = &mut [0u8;4];
                        BigEndian::write_i32(b, n);
                        v.extend(b.iter());
                    },
                    &Vars::FNum(n) => {
                        v.push(env::FNUM);
                        
                        let b = &mut [0u8;8];
                        BigEndian::write_f64(b, n);
                        v.extend(b.iter());
                    },
                    &Vars::Ref(ref r) => {
                        v.push(env::REF); // TODO impl ref kinds
                        
                        let b = &mut [0u8;4];
                        BigEndian::write_u32(b, r.value.chars().count() as u32);
                        v.extend(b.iter());
                        
                        v.extend(&r.value.clone().into_bytes()[..]);
                    },
                    &Vars::String(ref s) => {
                        v.push(env::STRING);
                        
                        let b = &mut [0u8;4];
                        BigEndian::write_u32(b, s.chars().count() as u32);
                        v.extend(b.iter());
                        
                        v.extend(&s.clone().into_bytes()[..]);
                    },
                    &Vars::Bool(ref b) => {
                        v.push(env::BOOL);
                        v.push(*b as u8); // NOTE: false is 0
                    },
                    &Vars::Byte(ref b) => {
                        v.push(env::BYTE);
                        v.push(*b);
                    },
                    &Vars::Vec(ref vec) => { // NOTE: may consider begin/end tags
                        v.push(env::VEC);

                        // write len of vec
                        let b = &mut [0u8;4];
                        BigEndian::write_u32(b,v.len() as u32);
                        v.extend(b.iter());
                        
                        for var in vec.iter() {
                            //let var = var.clone();
                            println!("vec var:{:?}",var);
                            //v.extend(Kinds::Sym(*var).to_bytes().iter());
                            
                        }
                        panic!("ni: VEC VAR!");
                    },
                    _ => { panic!("ni: byte encode for {:?}",sym); }
                }
            },
            
            /*&Kinds::QSym(ref s) => {
                v = vec!();
                v.push(QSYM);

                let b = &mut [0u8;4];
                BigEndian::write_u32(b, s.chars().count() as u32);
                // TODO: consider len instead of chars count?
                v.extend(b.iter());
                
                v.extend(&s.clone().into_bytes()[..]);
            },*/
            &Kinds::Sexp(ref x) => {
                v = vec!();
                
                for sexp in x.iter() {
                    v.extend(&sexp.to_bytes()[..])
                }
                
                v.push(SEXP_E);
                v.insert(0,SEXP_B);
            },
        }
        
        v
    }

    pub fn from_bytes (data: &[u8], cursor: &mut usize) -> Option<Kinds> {
        let header = data[*cursor];
        *cursor += 1;
        match header {
            SYM => {
                let header = data[*cursor];
                *cursor += 1;

                match header {
                    env::BOOL => {
                        let k = Kinds::Sym(Vars::Bool(data[*cursor] == 1)); // NOTE: 1 is true
                        *cursor += 1;
                        Some(k)
                    },
                    env::BYTE => {
                        let k = Kinds::Sym(Vars::Byte(data[*cursor]));
                        *cursor += 1;
                        Some(k)
                    },
                    env::INUM => {
                        let k = Kinds::Sym(Vars::INum(BigEndian::read_i32(&data[*cursor..*cursor+4])));
                        *cursor += 4;
                        Some(k)
                    },
                    env::FNUM => {
                        let k = Kinds::Sym(Vars::FNum(BigEndian::read_f64(&data[*cursor..*cursor+8])));
                        *cursor += 8;
                        Some(k)
                    },
                    env::REF => {
                        {
                            let size = BigEndian::read_i32(&data[*cursor..*cursor+4]) as usize;
                            let mut v = vec!();
                            *cursor += 4;
                            v.extend_from_slice(&data[*cursor..*cursor+size]);
                            // NOTE: may consider from_utf8_unchecked!
                            let k = Kinds::Sym(Vars::Ref(Ref::default(String::from_utf8(v).unwrap())));
                            *cursor += size;
                            Some(k)
                        }
                    },
                    env::STRING => {
                        {
                            let size = BigEndian::read_i32(&data[*cursor..*cursor+4]) as usize;
                            let mut v = vec!();
                            *cursor += 4;
                            v.extend_from_slice(&data[*cursor..*cursor+size]);
                            // NOTE: may consider from_utf8_unchecked!
                            let k = Kinds::Sym(Vars::String(String::from_utf8(v).unwrap()));
                            *cursor += size;
                            Some(k)
                        }
                    },
                    env::VEC => {
                        panic!("ni: VEC VAR");
                    },
                    _ => {
                        panic!("unsupported var type");
                    },
                }
            },
            /*QSYM => {
                let size = BigEndian::read_i32(&data[*cursor..*cursor+4]) as usize;
                let mut v = vec!();
                *cursor += 4;
                v.extend_from_slice(&data[*cursor..*cursor+size]);
                // NOTE: may consider from_utf8_unchecked!
                let k = Kinds::QSym(String::from_utf8(v).unwrap());
                *cursor += size;
                Some(k)
            },*/
            SEXP_B => {
                let mut v = vec!();
                let mut count = 0;

                if header == SEXP_B { count += 1; }
                
                loop {
                    
                    if data[*cursor] == SEXP_B { count += 1; }
                    else if data[*cursor] == SEXP_E { count -= 1; }
                    if count < 1 { break; }
                    
                    if let Some(r) = Kinds::from_bytes(data,cursor) {
                        v.push(r);
                    }
                }
                
                Some(Kinds::Sexp(v))
            },
            SEXP_E => None,
            _ => panic!("unknown header used! {:?}",header),
        }
    }

    pub fn to_string (&self) -> String {
        let mut s = String::new();
        
        match self {
            /*&Kinds::QSym(ref d) => {
                panic!("ni: qsym to string"); // we don't have this finalized
                s.push('"');
                s.push_str(d);
                s.push('"');
            },*/
            &Kinds::Sexp(ref v) => {
                s.push('(');
                s.push_str(&::parse::to_string(&v));
                s.push(')');
            }
            &Kinds::Sym(ref d) => {
                s.push_str(&d.to_string());
            },
        }
        
        s
    }
}



use std::fmt::{self, Formatter, Display};
impl Display for Kinds {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}",self.to_string())
    }
}
