/// actual evaluation from parsed

use ::env::{Env,Vars, Natives};
use ::core::{math,intrinsics};
use ::kinds::Kinds;

use std::mem;

#[derive(Debug,PartialEq)]
pub enum EvalVars<'a> {
    Borrow(&'a Vars),
    Own(Vars),
}

pub struct Eval;

impl Eval {
    pub fn find_var_base<'a> (env: &'a Vec<Env>, name: &str) -> Option<&'a Vars> {
        let mut name = name;
        loop {
            let v = Eval::find_var(env,name);
            if let Some(var) = v {
                match var {
                    &Vars::Ref(ref var_ref) => { name = var_ref.name(); continue },
                    _ => {},
                }
            }
            return v
        }
    }
    pub fn find_var<'a> (env: &'a Vec<Env>, name: &str) -> Option<&'a Vars> {
        for env in env.iter().rev() {
            if let Some(var) = env.vars.get(name) {
                let is_ref;
                match var {
                    &Vars::Ref(ref var_ref) => {
                        if let Some(var) = env.vars.get(var_ref.name()) {
                            return Some(var)
                        }
                        else { is_ref = true; }
                    },
                    _ => return Some(var),
                }

                if is_ref { return Some(var) }
            }
        }

        None
    }

    pub fn take_var (env: &mut Vec<Env>, name: &str) -> Option<Vars> {
        for env in env.iter_mut().rev() {
            if let Some(var) = env.vars.remove(name) {
                match var {
                    Vars::Ref(var_ref) => {
                        if let Some(var) = env.vars.remove(&var_ref.value) {
                            return Some(var)
                        }
                    },
                    _ => return Some(var),
                }
            }
        }

        None
    }

    pub fn take_var_base (env: &mut Vec<Env>, name: &str) -> Option<Vars> {
        let mut name = name.to_string();
        loop {
            let v = Eval::take_var(env,&name);
            if let Some(ref var) = v {
                match var {
                    &Vars::Ref(ref var_ref) => { name = var_ref.value.clone(); continue },
                    _ => {},
                }
            }
            return v
        }
    }

    // depth first eval
    pub fn depth<'a> (sexp: &Vec<Kinds>, mut scope: Vec<Env>,mut res: &'a mut Vars) -> Vec<Env> {
        let mut next = sexp;
        let mut stack: Vec<(usize, Vec<EvalVars>, &Vec<Kinds>)> = vec!();
        let mut next_start = 0;

        'outer: loop {
            if stack.len() < 1 { stack.push((0, vec!(), next)); }
            
            for (i,k) in next[next_start..].iter().enumerate() {
                
                { // handle control flow logic
                    let frames = &mut stack.last_mut().expect("no stack to push sym");
                    let vars = &mut frames.1;
                    let len = vars.len();
                    if len > 1 {
                        match &vars[0] {
                            &EvalVars::Borrow(&Vars::Native(Natives::If)) => {
                                if len == 2 {
                                    match &vars[1] {
                                        &EvalVars::Own(Vars::Bool(false)) |
                                        &EvalVars::Own(Vars::Nil) => {
                                            vars.push(EvalVars::Own(Vars::Nil));
                                            continue // continue on to final symbol in if-sexp
                                        },
                                        _ => {}
                                    }
                                }
                                else if len == 3 {
                                    match &vars[2] {
                                        &EvalVars::Own(Vars::Nil) => { },
                                        _ => { break } // means pred is true/non-nil and let's skip last eval
                                    }
                                }
                                else if len > 3 { break }
                            },
                            &EvalVars::Borrow(&Vars::Native(Natives::Proc)) => {
                                if len == 2 {
                                    match &vars[1] {
                                        &EvalVars::Borrow(&Vars::Vec(ref v)) => {
                                            let mut refs = vec!();
                                            for s in v {
                                                match s {
                                                    &Vars::Ref(ref r) => {
                                                        refs.push(r.clone());
                                                    },
                                                    _ => {}
                                                }
                                            }

                                            let mut exps = vec!();
                                            for s in &next[2..] {
                                                exps.push(s.clone());
                                            }
                                            
                                            vars.push(EvalVars::Own(Vars::Proc(refs,exps)));
                                            break
                                        },
                                        _ => {},
                                    }
                                    
                                    break
                                }
                            },
                            &EvalVars::Borrow(&Vars::Native(Natives::While)) => {
                                 match &vars[1] {
                                     &EvalVars::Own(Vars::Bool(false)) => {
                                         break
                                     },
                                     _ => { }
                                 }
                            },
                            &EvalVars::Borrow(&Vars::Native(Natives::Let)) => {
                                scope.push(intrinsics::let_bind(Env::new(),vars));
                            }
                            _ => {}
                        
                        }
                    }
                }
                
                match k {
                    &Kinds::Sexp(ref n) => {
                        stack.push((i+next_start, vec!(), next));
                        next_start = 0;
                        
                        next = n;

                        continue 'outer
                    },
                    &Kinds::Sym(ref s) => {
                            let frame = &mut stack.last_mut().expect("no stack to push sym").1;
                            frame.push(EvalVars::Borrow(s));
                    },
                }
            }
            
            let (frame_idx,mut last_val,new_next) = stack.pop().expect("no stack for frame eval");

            let mut len = 0;
            let mut is_let = false;
            if let Some(v) = last_val.get(0) {
                match v {
                    &EvalVars::Borrow(&Vars::Native(Natives::If)) |
                    &EvalVars::Borrow(&Vars::Native(Natives::Proc)) => {
                        len = last_val.len() - 1;
                        next_start = frame_idx + 1;
                    },
                    &EvalVars::Borrow(&Vars::Native(Natives::While)) => {
                        match &last_val[1] {
                            &EvalVars::Own(Vars::Bool(false)) => {
                                next_start = frame_idx + 1;
                            },
                            _ => { 
                                next_start = frame_idx;
                            }
                        }
                    },
                    &EvalVars::Borrow(&Vars::Native(Natives::Let)) => {
                        is_let = true;
                        next_start = frame_idx + 1;
                    },
                    _ => {
                        next_start = frame_idx + 1;
                    },
                }
            }

            next = new_next;
            
            if is_let {
                let result;
                let val = last_val.pop().expect("let drop value missing");
                let env = scope.pop().unwrap(); //this expects already layered env
                let mut env = vec!(env);
                match val {
                    EvalVars::Borrow(val) => {
                        match val {
                            &Vars::Ref(ref r) => {
                                if let Some(base) = Eval::take_var_base(&mut env,
                                                                        r.name()) {
                                    result = base;
                                }
                                else {
                                    result = Vars::Ref(r.clone());
                                }
                            },
                            _ => {
                                println!("warn: cloning let drop");
                                result = val.clone()
                            },
                        }
                    },
                    EvalVars::Own(val) => {
                        match val {
                            Vars::Vec(v) => { // we need to find dropped variables
                                let mut new_v = vec!();
                                for n in v {
                                    match n {
                                        Vars::Ref(r) => {
                                            if let Some(base) = Eval::take_var_base(&mut env,
                                                                                    r.name()) {
                                                new_v.push(base);
                                            }
                                            else {
                                                new_v.push(Vars::Ref(r.clone()));
                                            }
                                        },
                                        _=> { // NOTE: no support for nested vecs!
                                            new_v.push(n);
                                        }
                                    }
                                }
                                result = Vars::Vec(new_v);
                            },
                            _ => { result = val },
                        }
                    }
                }

               
                let frame = &mut stack.last_mut().expect("no stack to push sym").1;
                frame.push(EvalVars::Own(result));
                continue 'outer
            }
            else { last_val.drain(..len); }

            {
                let mut tmp = vec!();
                
                for n in &last_val[..] {
                    match n {
                        &EvalVars::Own(ref v) => { 
                            tmp.push(v);
                        },
                        &EvalVars::Borrow(v) => { 
                            tmp.push(v);
                        },
                    }
                }

                if last_val.len() > 1 { // FIXME: implicit sexp at root scope
                    scope = Eval::sexp(scope,&tmp, res); //reset env scope
                    
                    if res != &Vars::Nil {
                        let frame = &mut stack.last_mut().expect("no stack to push sym").1;
                        let t = mem::replace(res,Vars::Nil); //move var out of result
                        
                        frame.push(EvalVars::Own(t));
                    }
                }
            }
            
            if last_val.len() == 1 { // identity
                if let Some(v) = last_val.pop() {
                    match v {
                        EvalVars::Own(v) => { 
                            *res = v;
                        },
                        EvalVars::Borrow(v) => { 
                            *res = v.clone();
                        },
                    }
                }
                
            }

            if stack.len() < 1 { return scope }
        }
    }

    fn sexp<'a>(mut scope: Vec<Env>, sexp: &Vec<&'a Vars>, res: &mut Vars) -> Vec<Env> {
        scope.push(Env::new());

        // NOTE: I should stop passing res in to these
        // since they are no longer nested
        let mut new_scope = {
            match sexp[0] {
                &Vars::Native(Natives::Add) => { math::add(scope,sexp,res) }
                &Vars::Native(Natives::Mul) => { math::mul(scope,sexp,res) }
                &Vars::Native(Natives::Sub) => { math::sub(scope,sexp,res) }
                &Vars::Native(Natives::Div) => { math::div(scope,sexp,res) }

                &Vars::Native(Natives::LT) => { math::less_great_than(scope,sexp,false,res) }
                &Vars::Native(Natives::GT) => { math::less_great_than(scope,sexp,true,res) }

                &Vars::Native(Natives::Def) => { intrinsics::def(scope,sexp,res) }
                &Vars::Native(Natives::Prn) => { intrinsics::prn(scope,sexp) }
                
                &Vars::Native(Natives::Vec) => {
                    let mut v = vec!();
                    for n in &sexp[1..] { v.push((*n).clone()); }
                    *res = Vars::Vec(v);
                    scope
                }
                &Vars::Ref(_) => {
                    intrinsics::proc_bind(scope,sexp,res)
                }
                _ => { scope }
            }
        };

        new_scope.pop(); // remove temp env

        new_scope
    }
}
