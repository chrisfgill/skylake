use ::kinds::Kinds;
use ::env::{Vars,Natives};

// TODO: convert to static string instead of creating new strings
pub fn parse (s: &str) -> Vec<Kinds> {
    let mut sexp = vec!();
    sexp.push(vec!());

    let mut in_quote = false;
    let mut was_quote = false;

    let mut in_str = false;
    let mut was_str = false;
    
    let mut in_rem = false;
    
    
    let mut exp = String::new();
    
    for c in s.chars() {
        if !in_quote {
            if c == '\'' && !in_rem {
                in_str = !in_str;
                was_str = true;
            }
            else if !in_str {
                if c == '(' && !in_rem {
                    sexp.push(vec!());
                }
                else if c == ' ' ||
                    c == '\r' ||
                    c == '\n' ||
                    c == '\t' ||
                    c == ',' ||
                    c == ']' ||
                    c == ')' {
                        if c == '\n' || c == '\r' { in_rem = false; }
                        if in_rem { continue; }
                        
                        if exp.chars().count() > 0 {
                            let scope = sexp.last_mut()
                                .expect("Requires scope");
                            let var = {
                                if !was_quote && !was_str {
                                    Vars::parse(&exp)
                                }
                                else {
                                    Vars::String(exp)
                                }
                            };
                            
                            scope.push(Kinds::Sym(var));
                            exp = String::new();
                        }
                        was_quote = false;
                        was_str = false;

                        if c == ')' {
                            if let Some(exps) = sexp.pop() {
                                if exps.len() < 1 {
                                    println!("warn: invalid sexp format [empty] {:?}",exps);
                                    continue
                                }
                                
                                let scope = sexp.last_mut()
                                    .expect("Requires scope");
                                
                                scope.push(Kinds::Sexp(exps));
                            }
                        }
                        else if c == ']' {
                            if let Some(mut exps) = sexp.pop() {
                                if exps.len() < 1 {
                                    println!("warn: invalid sexp format [empty] {:?}",exps);
                                    continue
                                }
                                
                                let scope = sexp.last_mut()
                                    .expect("Requires scope");

                                exps.insert(0,Kinds::Sym(Vars::Native(Natives::Vec)));
                                scope.push(Kinds::Sexp(exps));
                            }
                        }
                    }
                else if c == '\"' && !in_rem {
                    in_quote = true;
                }
                else if c == ';' { in_rem = true; continue; }
                else if c == '[' && !in_rem && !in_quote { sexp.push(vec!()); }
                else if !in_rem { exp.push(c); }
            }
            else { exp.push(c); }
        }
        else {
            if c == '\"' && !in_rem {
                in_quote = false;
                was_quote = true;
            }
            else { exp.push(c); }
        }
    }
    
    return sexp.pop().unwrap()
}

// TODO: provide new lines and formatting
pub fn to_string (data: &Vec<Kinds>) -> String {
    let mut s = String::new();
    
    for sym in data.iter() {
        s.push_str(&sym.to_string());
        s.push(' ');
    }

    s.pop(); // pop off remainder space
    
    s
}


pub fn to_bytes (data: &Vec<Kinds>) -> Vec<u8> {
    let mut v = vec!();
    for n in data.iter() {
        let mut x = n.to_bytes();
        for j in x.drain(..) {
            v.push(j);
        }
    }

    v
}

pub fn from_bytes (data: &Vec<u8>) -> Vec<Kinds> {
    let mut cursor = 0;
    let mut v = vec!();

    loop {
        if let Some(r) = Kinds::from_bytes(&data,&mut cursor) {
            v.push(r);
        }

        if cursor >= data.len() { break }
    }

    v
}
