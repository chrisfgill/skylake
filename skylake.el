;; skylake compilation
(defun sky-eval ()
  (interactive)
  (save-buffer)
  (compile
   (format "skylake.exe %s"
	   (buffer-file-name))))
(defvar sky-mode-hook nil)
(add-hook 'sky-mode-hook
	  (lambda ()
	    (define-key sky-mode-map (kbd "<f5>") 'sky-eval)))

(defgroup sky-mode nil
  "skylake mode"
  :link '(url-link "https://bitbucket.org/viperscape/skylake")
  :group 'languages)

(defcustom sky-indent-offset 2
  "indent by 2 spaces"
  :type 'integer
  :group 'sky-mode
  :safe #'integerp)


(add-to-list 'auto-mode-alist '("\\.sky\\'" . sky-mode))
(define-derived-mode sky-mode scheme-mode "skylake"
  (rainbow-delimiters-mode)
  (company-mode))
